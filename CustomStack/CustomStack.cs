﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace CustomStack
{
    public class CustomStack<T> : Stack<T>, INotifyCollectionChanged
    {
        public new Type GetType()
        {
            return typeof(T);
        }

        public new void Push(T item)
        {
            base.Push(item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        public new T Pop()
        {
            var result = base.Pop();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, result));
            return result;
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        private void OnCollectionChanged(NotifyCollectionChangedEventArgs eventArgs)
        {
            CollectionChanged?.Invoke(this, eventArgs);
        }
    }
}
